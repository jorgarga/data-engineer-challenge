#include <stdio.h>
#include <stdlib.h>
 
void main(int argc, char *argv[] ) {

    if(argc != 2) {
        printf("Argument missing\n");
        return;
    }

    FILE *stream;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
    int count = 0;

    printf("Opening file: %s\n", argv[1]);

	stream = fopen(argv[1], "r");
	if (stream == NULL)
		exit(EXIT_FAILURE);
 
	while ((read = getline(&line, &len, stream)) != -1) {
        count++;
	}

    printf("Lines processed: %d\n", count);
	free(line);
	fclose(stream);
	exit(EXIT_SUCCESS);
} 
