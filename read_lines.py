# coding: utf-8
import os
import logging

import argparse
import hashlib
import ntpath
import datetime

import concurrent.futures

logging.basicConfig(filename="application.log",
                    filemode="a",
                    level=logging.DEBUG,
                    format="%(asctime)s - %(levelname)s - %(message)s")


ALL_FIELDS = ["imsi", "imei", "cell_id", "tac",
              "rac", "sac", "area_id", "segment",
              "ts_start", "ts_end", "erm", "trf"]

# NOTE:
# In a real-life scenario this data would be sent as parameter in a
# config file, for example. To keep things simpler to follow it is used here.
IMSIS = ["CE9E62301DDCE626128B708220D2498D34C38F0CEAC753DE4836B426A0747A29",
         "0DF96B53E1BD1B4CD1377CE2DCF7F12BB9384F915E2F12D1BBC4E5E5B2668B90",
         "7A3751D3534BD8E50E69AFC6A1D3E40B5D14FC33B12333DC7649A05AD224971C",
         "149394F92C7B6E07D66C792A68B0F232064FE9EFF5D873DA1486E81280592A48"]


def imsis_as_set(imsis: list) -> set:
    """
    This function transforms a list containing IMSI into a set.

    Note: A set is desirable in this case (rather than a list) because
    it has a constant searching time. This function will be used a lot, for
    each existing line on each file. It has to be as fast a possible.

    :param imsis: List of IMSIs elements one is interested in.
    :returns: A set containing the IMSIs one is interested in.
    """

    return set(map(lambda imsi: imsi.lower(), imsis))


def get_files(origin: str) -> list:
    """
    Returns a list with the CSV files in the path given as origin.
    All non-CSV files are ignored.

    :param origin: String representing the path where the CSV files
                   are located.
    :returns: List of CSV files.
    """

    def is_csv(filename):
        return filename.endswith(".csv") or filename.endswith(".CSV")

    # This list only contains CSV files.
    csv_files = filter(is_csv, os.listdir(origin))

    return list(
        map(lambda filename:
            os.path.join(origin, filename),
            csv_files)
    )


def applicable(line: str, imsis: set) -> str:
    """
    This function operates with lines and returns a valid line or False.
    A valid line is one which has **all** the fields AND which hashed
    IMSI is in the set of interesting IMSIs that is given as a parameter.

    The line returned will have a hashed IMSI instead of the plain imsi.
    This step simulates anonymization.

    The line returned will have datetimes instead of timestamps.

    NOTE: This function needs to be keept as fast as possible since
          the program is going to call it for each line on each file.

    :param line: String representing a line from the CSV file.
    :param imsis: Set containing the imsis one is interested in.
    :returns: String representing a line with anonimyzed IMSI if line is valid.
              False if line is not valid.
    """

    def get_hash(imsi: str) -> str:
        """
        Auxiliar function to anonimyze the imsi value.
        """
        return hashlib.sha256(imsi.encode("utf-8")).hexdigest()

    def get_datetime(timestamp: str) -> str:
        """
        Auxiliar function which converts timestamp to datetime. Human readable.
        """
        return datetime.datetime.fromtimestamp(
            int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')


    if len(line) == 0:
        return False

    try:
        parts = line.split(",")
        # Drop the line if it does not contain all the fields.
        if len(parts) != len(ALL_FIELDS):
            return False

        # Substitute IMSI with the hashed version of IMSI.
        parts[0] = get_hash(parts[0])

        # The line will be ignored if the hash is not in our set
        if parts[0] not in imsis:
            return False

        # Keep the columns which are interesting for us:
        processed = "{},{},{},{},{}".format(parts[0],  # hashed imsi
                                            parts[2],  # cell_id
                                            parts[6],  # area_id
                                            get_datetime(parts[8]),  # ts_start
                                            get_datetime(parts[9]))  # ts_end
        return processed + "\n"
    except Exception as ex:
        raise ex


def process(origin: str, destpath: str, imsis: set):
    """
    This function controls the processing of a file.
    The origin file will be read line by line. Each line will be passed
    to "applicable" which tests for correctness and makes some transformations.
    Any problems found by "applicable" will be reported on the log.


    :param origin: String representing the CSV value to process.
    :param destpath: String representing a path where the processed
                     result will be saved.
    :param imsis: Set of IMSIs one is interested in.

    :returns: This function will write the processed lines into a
              file saved to destpath with the same filename as the origin file.
    """

    basename = ntpath.basename(origin)
    dest = os.path.join(destpath, basename)

    logging.debug("File to process: {} will be saved on {}."
                  .format(origin, destination))

    with open(dest, "w", encoding="utf-8") as output, open(origin) as infile:
        for line in infile:
            try:
                result = applicable(line, imsis)
                if not result:
                    continue

                output.write(result)
            except Exception as ex:
                message = "File {} - Line: {} - Exception: {}"
                logging.warning(message.format(origin, line, ex))

    logging.info("Processing done for file: {}".format(origin))


if __name__ == "__main__":
    logging.info("Program started")

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--origin",
                        help="Folder with CSV files to process.",
                        required=True)
    parser.add_argument("-d", "--destination",
                        help="Folder where the results will be written.",
                        required=True)
    args = parser.parse_args()

    origin = args.origin
    destination = args.destination

    if not os.path.exists(destination):
        os.makedirs(destination)

    files_origin = get_files(origin)
    path_destination = [destination] * len(files_origin)
    interesting_imsis = [imsis_as_set(IMSIS)] * len(files_origin)

    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(process,
                     files_origin,
                     path_destination,
                     interesting_imsis)

