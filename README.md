Data engineer challenge
--------

In this document I will describe how I proceeded to solve the challenge.
The strategy taken and the reason.

**DISCLAIMER**:
>This is an exercise I got for a data engineer position.
> I am not publishing neither the data given for the exercise nor the result, just how I solved the problem.
> This is mainly as documentation for me and for others to look at my portafolio/software skills and interests.


# The exercise
Download some data from different sources:
- File XXXXX.tgz from SFTP.
- File de.sqlite from [radiocells](https://radiocells.org/).

Do the following:
- Process the signaling information.
  - Filter the necessary columns.
  - Filter the necessary rows.
  - Simulate the privacy step hashing the imsi column.
- Join it with the cell information.
- Visualize a few selected movement chains.


# 1. Experimenting
The next step consists on exploring the files to get an idea of the data:
- How it looks like.
- How much data it is.

With Linux command tools (head, tail, less) one can take a brief look.
There are 9 CSV files each about 1,9 GB. In total there are 122995448 lines taking 17,3 GB.

The exercise claims to be "designed to be solved on a normal PC". Since the concept of *normal PC* is unclear, I will present the specific hardware resources used to solve the challenge:
- 4x CPU @ 3.10GHz.
- 15 GB RAM available for user applications.
- Solid-State-Drive.

Since the data given is bigger than the amount of RAM in the system [Pandas](https://pandas.pydata.org/) is discarded here as it would perform poorly.
Even with more RAM the performance would not be the best since Pandas does not use parallelism at all.

The following lines presented in the exercise keep me thinking:
> However efficient utilization of the CPU, memory and disk operations will be required to develop
> an application which finished within a reasonable time.
> When we say reasonable we do not mean nanoseconds or seconds.
> Anything **bellow an hour or around it** should be fine.
> If you find your application running for hours, then it might need some more tuning ☺

Instead of using a framework I decide to write some code which will process each line.
To proof that this _can_ be a good way to proceed I need to get an idea of the fastest time to iterate over all the lines.
If this time was -let's say-  50 minutes -for the sake of the argument- then I would know that it is **not** the way to proceed.
The fastest is usually code written in C, so I wrote a simple program to iterate over a file without loading the complete file into memory.
Since the program runs in 0,3 seconds I am confident that this approach can help me solve the first part of the exercise.

Similar code written in Python shows a time of about 4 seconds to just iterate though a file.
That is more than 10 times slower than the C counterpart. But in reality 4 seconds is not such a big deal and Python enables me to code faster.
Because of that and because it is the language used by the company I will code in Python.


# 2. Preprocessing the signaling data
As mentioned in the previous paragraph one will use Python to process the signalig data.
The task is to:
- Filter the necessary columns.
- Filter the necessary rows.
- Simulate the privacy step hashing the imsi column.


## Implementation idea
The implementation will follow a _functional_ approach, meaning that one will concentrate on doing the job for one file and later on start jobs on different cores.
The system has many processors after all. Let's use them.
The most important step is filtering rows. It is important to filter rows:
- which do not contain all the data/are empty.
- whose hashed IMSI value is not present in the list given on the exercise.

The main goal is to reduce the amoung of data as **aggresively** as possible. Later on "data joins" will be needed between the phone data and the cell data.
Join is an operation with a high computational cost, so the less data to join the faster it will run.


## Running the application


### Environment
My system uses a Python distribution called [Anaconda](https://www.anaconda.com/download/).
Before running the application a virtual environment should be created. Python 3.6.5 has been used.
> python3 -m venv venv

Start/activate a virtual environment
> source venv/bin/activate

Install needed requirements.
> pip3 install -r requirements.txt

The packages used in this project are:
- Pandas
- Jupyter
- Folium


### Run and results
Run the application with the following command:
> time python read_lines.py -o ./data_v1 -d ./data_processed

The processed data will be placed in the *data_processed* directory.

While running the application the system is carefully monitored with these tools:
- top   (processor use).
- iotop (disk use).
- free  (memory use).

The 4 cores are used for processing the files. Memory use is kept low as expected because the code does not load entire files.
Disk I/O is kept active while files are being read.

Additionally the application log is watched to verify that no problems appear:
> tail -f application.log

The result of the execution yields this information:
```sh
    real    2m36,398s
    user    8m30,190s
    sys     0m8,707s
```


## Scaling
The different types of scaling are:
- Vertical scaling: How to do more with just one machine.
- Horizontal scaling: Share the load with other machines.


### Vertical scaling
Some strategies to enable a single machine process more data:
- Create an [extension module](https://medium.com/practo-engineering/execute-python-code-at-the-speed-of-c-extending-python-93e081b53f04) in C which processes the data. Use it from Python.
- Compile the complete Python script with [Numba](http://numba.pydata.org/) or [Nuitka](http://nuitka.net/pages/overview.html).
- Add more processors to the cluster, since the _executor.map_ function uses all the processor's cores.
- Add faster disks, since disk access probably is a bottleneck here.


### Horizontal scaling
When it comes to horizontal scalability, using Apache [Spark](https://spark.apache.org/) seems a sound and stable option.
This software holds at the time of writting all the current speed records in processing, mainly because:
- It parallelizes as many opertations as possible (getting over Python GIL).
- It heavily relays on memory, which is orders of magnitude faster than disk access.


# 3. Joining the data
The result of the previous phase yields some CSV files in the *data_processed* directory.
For simplicity I will reuse the sqlite database given as input.
In a real-life scenario I would:
- Use a database server (Postgresql or MariaDB).
- Create a table index to search/join faster.


## Start SQLite command line tool
> $ sqlite3 de.sqlite
```sh
SQLite version 3.23.1
Enter ".help" for usage hints.


## Create a table for the CSV data
sqlite> CREATE TABLE user_positions(
   ...> hashed_imsi TEXT NOT NULL,
   ...> cid TEXT NOT NULL,
   ...> area TEXT NOT NULL,
   ...> start_time TEXT NOT NULL,
   ...> end_time TEXT NOT NULL
   ...> );
```

Verify that the table has been created:
> sqlite> .tables


## Import CSV data
Import the data from the processed files into the newly created table:
```sh
sqlite> .mode csv
sqlite> .import ./data_processed/1.csv user_positions
sqlite> .import ./data_processed/2.csv user_positions
sqlite> .import ./data_processed/5.csv user_positions
sqlite> .import ./data_processed/6.csv user_positions
```

Again, verify that has been imported correctly:
```sh
sqlite> SELECT * FROM user_positions LIMIT 4;
hashed_imsi,cid,area,start_time,end_time
0df96b53e1bd1b4cd1377ce2dcf7f12bb9384f915e2f12d1bbc4e5e5b2668b90,43202,301,"2018-09-01 20:16:48","2018-09-01 20:17:14"
0df96b53e1bd1b4cd1377ce2dcf7f12bb9384f915e2f12d1bbc4e5e5b2668b90,132643361,40023,"2018-09-01 21:35:22","2018-09-01 21:35:31"
0df96b53e1bd1b4cd1377ce2dcf7f12bb9384f915e2f12d1bbc4e5e5b2668b90,15434269,21935,"2018-09-01 13:47:25","2018-09-01 13:47:50"
0df96b53e1bd1b4cd1377ce2dcf7f12bb9384f915e2f12d1bbc4e5e5b2668b90,15434269,21935,"2018-09-01 14:26:57","2018-09-01 14:27:11"

sqlite> SELECT COUNT(*) FROM user_positions;
COUNT(*)
96
```


## Join cell information with user information
Join the tables and save the result into a CSV file for later processing (visualization):

```sh
.mode csv
.output joined_data.csv

SELECT up.hashed_imsi, up.cid, up.area, up.start_time, up.end_time, filtered_cz.latitude, filtered_cz.longitude, filtered_cz.LastUpdated
FROM (
        SELECT cell_zone.cid, cell_zone.area, cell_zone.latitude, cell_zone.longitude, MAX(cell_zone.last_updated) as LastUpdated
        FROM cell_zone
        GROUP BY cell_zone.cid, cell_zone.area
     ) filtered_cz
INNER JOIN user_positions up ON filtered_cz.area == up.area AND filtered_cz.cid == up.cid;

.output stdout
```

The trick here is to get the cell zone with the lastest updated information. This is accomplished by the inner subquery, which groups by cell id/area and chooses the max *last_updated* value.
I pressume the mobile posts' position is change from time to time...
The result of the previous query needs to be joined with the data in the user_positions table so that one can relate each movement of the user with a set of coordinates on the map.

I created a new folder and moved the file to that folder:
> mkdir visualization
> mv joined_data.csv ./visualization

Now one can move to the last step of the exercise.


# 4. Visualization
For this part of the exercise I will use a Jupyter notebook. Start the server in the shell:
> $> jupyter notebook

A browser window/tab will appear, follow the intructions if any.
Browse to the folder *visualization* and select the file *visualization.ipynb*.
The notebook will show code and the results of its execution. If wanted, the code can be rerun:
- Choose *Kernel* in the main menu
- Choose *Restart & Run All*

The cells may be run independently by hitting "shift + enter" keys.
Cell number 7 will show the map, each color represents each of the hashed imsis.
The last cell saves the result into an independent html file. This html file has been used for getting the images.
The final result, the images, can be found inside the directory *final_result*.


# Final comments
It has been an original exercise. It kept me thinking about approaches and it was fun to implement.
I want to thank you -the reader- for the time you spent reading my notes and thoughts about the challenge.
